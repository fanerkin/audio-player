# audio player

### features
* knockoutjs component
* composite
* async loading via requirejs

### usage
* registers itself as
```
<audio-player params=""></audio-player>
```
tag. To see an example:
```
$ cd /path/to/index.html
$ python -m SimpleHTTPServer 8000
```
then http://localhost:8000 in browser

### params
* tracks: comma-separated urls to files, the first one is used in minimal version
