define(function(require) {
	var
		ko = require('knockout'),
		core = require('./smooth-core'),
		parts = [
			require('./indicators'),
			require('./slider'),
			require('./controls')
		],
		template = parts.reduce(function (base, el) {
			return base + el.template;
		}, '');

	function AudioPlayerVM(params) {
		var self = this;

		if (!params.tracks) params.tracks = '';

		[core].concat(parts).forEach(function (part) {
			part.viewModel.call(self, params);
		});
		self.src(params.tracks.split(',')[0]);
	}

	AudioPlayerVM.prototype = Object.create(core.viewModel.prototype);

	parts.forEach(function (part) {
		Object.keys(part.viewModel.prototype).forEach(function (key) {
			AudioPlayerVM.prototype[key] = part.viewModel.prototype[key];
		});
	});

	return { viewModel: AudioPlayerVM, template: template };
});
