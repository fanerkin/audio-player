define(function(require) {
	var
		ko = require('knockout'),
		template = require('text!./templates/slider.html');

	function SliderVM() {}

	SliderVM.prototype = {
		change: function (vm, ev) {
			var range = ev.target;

			this.setTime(range.value);
			range.blur();
		}
	};

	return { viewModel: SliderVM, template: template };
});
