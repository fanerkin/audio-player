define(function(require) {
	var
		core = require('./core'),
		CoreVM = core.viewModel,
		template = core.template;

	function SmoothVM() {
		CoreVM.apply(this, arguments);
	}

	SmoothVM.prototype = Object.create(CoreVM.prototype, {
		onplaying: {
			value: function () {
				this.interval = setInterval(function () {
					this.ontimeupdate();
				}.bind(this), 50);
				CoreVM.prototype.onplaying.apply(this, arguments);
			}
		},
		onpause: {
			value: function () {
				clearInterval(this.interval);
				CoreVM.prototype.onpause.apply(this, arguments);
			}
		}
	});

	return { viewModel: SmoothVM, template: template };
});
