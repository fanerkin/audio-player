define(function(require) {
	var
		ko = require('knockout'),
		template = require('text!./templates/indicators.html');

	function format(time) {
		var
			minutes,
			seconds;

		function fixDigits(n) {
			return ( n < 10 ? '0' : '' ) + n;
		}

		minutes = Math.floor(time / 60);
		seconds = (time % 60).toFixed(2);

		return minutes + ':' + fixDigits(seconds);
	}

	function IndicatorsVM() {
		var self = this;

		self.indicators = [
			{
				css: function () {return 'track-length'},
				text: function () {return 'track length:'},
				value: function () {
					return self.duration() ?
						format(self.duration()) :
						'';
				}
			},
			{
				css: function () {return 'current-position'},
				text: function () {return 'current position:'},
				value: function () {
					return self.duration() ?
						format(self.position()) :
						'';
				}
			}
		];
	}

	return { viewModel: IndicatorsVM, template: template };
});
