define(function(require) {
	var
		ko = require('knockout'),
		template = require('text!./templates/controls.html');

	function ControlsVM() {
		var self = this;

		self.controls = [
			{
				css: function () {
					return self.isPaused() ?
						'paused' :
						'playing';
				},
				text: function () {
					return self.isPaused() ?
						'play' :
						'pause';
				},
				click: self.togglePaused.bind(self)
			},
			{
				css: function () {
					return self.isMuted() ?
						'muted' :
						'unmuted';
				},
				text: function () {
					return self.isMuted() ?
						'unmute' :
						'mute';
				},
				click: self.toggleMuted.bind(self)
			}
		];
	}

	return { viewModel: ControlsVM, template: template };
});
