define(function(require) {
	var
		ko = require('knockout'),
		audioEvents = {
			durationchange: function () {
				this.duration(this.audio.duration);
			},
			canplaythrough: function () {
				if (!this.isPaused()) {
					this.audio.play();
				}
			},
			timeupdate: function () {
				this.position(this.audio.currentTime);
			},
			pause: function () {
				this.isPaused(this.audio.paused);
			},
			playing: function () {
				this.isPaused(this.audio.paused);
			},
			volumechange: function () {
				this.isMuted(this.audio.muted);
			},
			ended: function () {
				// this.audio.play();
			},
			error: function () {
				this.ondurationchange();
			}
		};

	function CoreVM() {
		var self = this;

		self.audio = new Audio();
		self.src = ko.observable();
		self.isPaused = ko.observable(true);
		self.isMuted = ko.observable(false);
		self.duration = ko.observable();
		self.position = ko.observable(0);

		Object.keys(audioEvents).forEach(function (key) {
			self.audio['on' + key] = self['on' + key].bind(self);
		});

		self.src.subscribe(function (src) {
			self.audio.src = src;
		});
	}

	CoreVM.prototype = {
		setTime: function (value) {
			this.audio.currentTime = value;
		},
		play: function () {
			this.audio.play();
		},
		pause: function () {
			this.audio.pause();
		},
		togglePaused: function () {
			if (this.audio.paused) this.play();
			else this.pause();
		},
		toggleMuted: function () {
			this.audio.muted = !this.audio.muted;
		}
	};

	Object.keys(audioEvents).forEach(function (key) {
		CoreVM.prototype['on' + key] = audioEvents[key];
	});

	return { viewModel: CoreVM, template: '' };
});
