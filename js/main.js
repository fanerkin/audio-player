requirejs.config({
	paths: {
		knockout: './lib/knockout',
		text: './lib/text'
	}
});

define(function (require) {
	'use strict';

	var ko = require('knockout'),
		audioPlayer = require('./components/audio-player/player');

	ko.components.register(
		'audio-player',
		audioPlayer
	);

	ko.applyBindings();
});
